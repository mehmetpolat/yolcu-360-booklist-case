/**
 * Fiyatlari formatlamak icin kullanilir.
 *
 * Formatlamayi yolcu360'a bakarak yaptim, umarim dogrudur :)
 *
 * @param {number} amount - Formatlanacak tutar.
 * @returns {string} - Tutari string olarak doner
 */
export const amountFormatter =  (amount) => {
    console.log('a', amount)
    if(!amount){
        return '0,00'
    }
    return new Intl.NumberFormat('tr-TR', { minimumFractionDigits:2, maximumFractionDigits: 6, useGrouping: true }).format(amount).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
