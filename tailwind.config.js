/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
    "./app.vue",
  ],
  theme: {
    fontFamily: {
      Poppins: ["Poppins", "sans-serif"],
    },
    spacing: {
      '0': '0',
      '1': '0.25rem',
      '2': '0.5rem',
      '3': '0.75rem',
      '4': '1rem',
      '5': '1.25rem',
      '6': '1.5rem',
      '7': '2rem',
      '8': '2.5rem',
      '9': '3rem',
      '10': '4rem',
      '11': '4.5rem',
      '12': '5rem',
      '13': '5.5rem'
    },

    container: {
      center: true,
      padding: '2rem',
    },
    fontWeight: {
      light: '300',
      normal: '400',
      medium: '500',
      bold: '600',
    },
    fontSize: {
      'tiny': ["0.75rem", "1rem"],
      'sm': ["0.85rem", "1rem"],
      'md': ["0.95rem", "1.12rem"],
      'base': ["1rem", "1.375"],
      'lg': ["1.25rem", "1.625rem"],
      'xl' : ['1.5rem', '2rem'],
      '2xl': ['2rem', '2.5rem'],
      '3xl': ['3rem', '3.875rem'],
      '4xl': ['4rem', '4.75rem']
    },
    extend: {
      colors: {
        'primary': {
          DEFAULT: '#ECF0F1',
          50: '#FFFFFF',
          100: '#FFFFFF',
          200: '#FFFFFF',
          300: '#FFFFFF',
          400: '#FFFFFF',
          500: '#ECF0F1',
          600: '#CCD7D9',
          700: '#ABBDC1',
          800: '#8BA3AA',
          900: '#6B8A91',
          950: '#5F7A81'
        },

      },
      minWidth: {
        '0': '0',
        '1': '0.25rem',
        '2': '0.5rem',
        '3': '0.75rem',
        '4': '1rem',
        '5': '1.25rem',
        '6': '1.5rem',
        '7': '2rem',
        '8': '2.5rem',
        '9': '3rem',
        '10': '4rem',
        '11': '4.5rem',
        '12': '5rem',
        '13': '5.5rem'

      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    // ...
  ],
}

