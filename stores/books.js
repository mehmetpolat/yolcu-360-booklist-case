import { defineStore } from 'pinia'

export const useBookStore = defineStore('book', () => {
    const allBooks = ref([])
    const searchResult = ref([])

    const setBookList = (books) => {
        allBooks.value = books
        searchResult.value = books
    }

    const getBookList = computed(() => {
        return searchResult
    })

    const searchBooks = (keyword) => {
        if (keyword === '') {
            searchResult.value = allBooks.value
            return
        }

        searchResult.value = allBooks.value.filter(
            (book) =>
                book.author.toLowerCase().includes(keyword) ||
                book.title.toLowerCase().includes(keyword)
        )
    }

    return {
        allBooks,
        searchResult,
        setBookList,
        searchBooks,
        getBookList
    }
})
