import { defineStore } from 'pinia'
export const useCartStore = defineStore('cart', () => {
    const cartItems = ref([])
    const totalCount = ref(0)
    const totalPrice = ref(0)
    const getCartItems = computed(() => cartItems.value )
    const getTotalCount = computed(() => totalCount.value )
    const getTotalPrice = computed(() => totalPrice )

    const addNewItem = (newItem) => {
        // NewItem olarak verilen bir öğeyi alır ve onu sepet öğeleri listesine ekler. Yeni eklenen öğe, count özelliği 1 olarak ayarlanarak tanımlanır.
        return cartItems.value.push({
            ...newItem,
            count: 1
        })
    }
    const decreaseCountByItem = (id) => {
        // Sepetteki urunun adetini azaltir
        const item = cartItems.value.find(item => item.id === id)
        if (item) {
            item.count -= 1
        }
        if(item.count === 0) {
            cartItems.value = cartItems.value.filter(item => item.id !== id)
        }
        calculateTotalCount()
        calculateTotalPrice()
    }
    const increaseCountByItem = (id) => {
        // Sepetteki urunun adetini arttirir
        const item = cartItems.value.find(item => item.id === id)
        if (item) {
            item.count += 1
        }
        calculateTotalCount()
        calculateTotalPrice()
    }
    const deleteItem = (id) => {
        cartItems.value = cartItems.value.filter(item => item.id !== id)
        calculateTotalCount()
        calculateTotalPrice()
    }
    const calculateTotalCount = () => {
        // Sepetteki urunlerin toplam adet sayisini hesaplarim
        totalCount.value = cartItems.value.reduce((acc, book) => acc + book.count, 0);
    }
    const calculateTotalPrice = () => {
        // Sepetteki urunlerin toplam fiyatlarini hesaplarim
        totalPrice.value = cartItems.value.reduce((acc, book) => acc + book.price * book.count, 0);
    }
    const addCartItem = (newItem) => {
        // Eger sepet icerisinde urunler varsa ayni urunden sepette var mi kontrol eder, eger varsa urunun count degerini arttirir yoksa yeni urun ekler
        // ornek olsun diye shortland if blogu kullandim ama cok onerilen bir yontem degil
        const foundItem = cartItems.value.find(item => item.id === newItem.id)
        foundItem ? foundItem.count++ : addNewItem(newItem)

        calculateTotalCount()
        calculateTotalPrice()
    }

    return {
        addCartItem,
        getCartItems,
        getTotalCount,
        getTotalPrice,
        decreaseCountByItem,
        increaseCountByItem,
        deleteItem
    }
})
