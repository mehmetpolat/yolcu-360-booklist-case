# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Requirement
-	Node.js v16.10.0 or newer. https://nodejs.org/en/download/
### Folder Structure:
 
- `node_modules` - dependencies
- `pages/` - routes
- `components/` - component files
- `assets/` - scss files, images, and fonts
- `layouts/` - Layouts templates
- `public/` - Files that will not change (robots.txt etc.)
- `helpers/` - Helper Files
- `test/` - Test Files

## Setup

Clone this repo:

```bash
git clone https://gitlab.com/mehmetpolat/yolcu-360-booklist-case
```

Navigate into the repo directory:

```bash
cd yolcu-360-booklist-case
```

install the dependencies::

```bash
# yarn
yarn install

# npm
npm install

# pnpm
pnpm install

```


## Development Server

Start the development server on `http://localhost:3000`

```bash
npm run dev
```
## Start Testing

Start the development server on `http://localhost:3000`

```bash
npm run unit:test
```

## Production

Build the application for production:

```bash
npm run build
```

Locally preview production build:

```bash
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
