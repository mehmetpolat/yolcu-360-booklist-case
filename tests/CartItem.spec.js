import { describe, test, expect, vi } from 'vitest'
import { mount } from "@vue/test-utils";
import { createTestingPinia } from '@pinia/testing'
import {useCartStore} from "@/stores/cart";

import BookListItem from '@/components/bookList/Item/index.vue'
describe("BookListITem", () => {
    const errorImageMock = vi.fn()
    const item = {
        id: 1,
        title: "Book Title",
        author: "Book author",
        price: 12.99,
        coverImageUrl: 'example'
    }
    const wrapper = mount(BookListItem, {
        props: {
            item
        },
        methods: {
            errorImage: errorImageMock
        }
    })

    test('renders book title is true', () => {
        const bookTitle = wrapper.get('[data-test="bookTitle"]')
        expect(bookTitle.text()).toBe(item.title)
    })
    test('renders book author is true', () => {
        const bookAuthor = wrapper.get('[data-test="bookAuthor"]')
        expect(bookAuthor.text()).toBe(item.author)
    })
    test('renders book price is true', () => {
        const bookPrice = wrapper.get('[data-test="bookPrice"]')
        expect(parseFloat(bookPrice.text())).toBe(item.price)
    })
    test('renders an error message when image src is invalid', () => {
        const img = wrapper.find('img')
        img.trigger('error')
        const newImage = img.attributes('src');
        expect(newImage).toBe('https://static.vecteezy.com/system/resources/previews/005/337/799/large_2x/icon-image-not-found-free-vector.jpg')
    })
});

