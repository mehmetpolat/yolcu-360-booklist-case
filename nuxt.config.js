// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    ssr: false,
    runtimeConfig: {
        public: {
            API_URL: process.env.API_URL
        }
    },
    css: ['~/assets/styles/main.scss'],
    modules: [
        // ...
        'nuxt-vitest',
        '@pinia/nuxt',
    ],

    postcss: {
        plugins: {
            tailwindcss: {},
            autoprefixer: {},
        },
    },
})
