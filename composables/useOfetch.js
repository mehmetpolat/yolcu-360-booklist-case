/**
 * useFetch kancasını kullanarak belirtilen URL'ye bir getirme isteği gönderir ve istek ve yanıt olaylarını yonetir.
 *
 * @param {string} url - Fetch istegi atilacak path
 * @param {object} [options] - useFetch hookuna aktarılacak ek seçenekler.
 * @returns {object} - useFetch hooku tarafından döndürülen yanıt nesnesi.
 */
export const useOFetch = (url, options = {}) => {
    const config = useRuntimeConfig()
    return useFetch(`${config.public.API_URL}${url}`, {
        ...options,
        async onResponse({ request, response, options }) {

            console.log('[fetch response]')
        },
        async onResponseError({ request, response, options }) {
            console.log('[fetch response error]')
        },

        async onRequest({ request, options }) {
            console.log('[fetch request]')
        },
        async onRequestError({ request, options, error }) {
            console.log(error)
        }
    })
}
